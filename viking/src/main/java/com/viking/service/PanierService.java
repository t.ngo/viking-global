package com.viking.service;

import java.util.List;

import com.viking.entity.Article;
import com.viking.entity.Panier;

public interface PanierService {

	public void createPanier(Panier panier);
	public void deletePanier(long id);
	public void addArticle(long id, Article art);
	public void removeArticle(long id, long idArt);
	public void clearPanier(long id);
	public Panier getPanier(long idPanier);
	public void validatePanier(Long idPanier);
	public List<Article> getPanierList(long id);

}
