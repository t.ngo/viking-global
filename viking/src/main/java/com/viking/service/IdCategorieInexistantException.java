package com.viking.service;

public class IdCategorieInexistantException extends Exception {

	private static final long serialVersionUID = 1L;

	public IdCategorieInexistantException() {
		super("La catégorie n'existe pas ! ");
	}
	
}
