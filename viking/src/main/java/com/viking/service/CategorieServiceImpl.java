package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.CategorieDao;
import com.viking.entity.Categorie;

@Service
public class CategorieServiceImpl implements CategorieService{
	
	@Autowired
	private CategorieDao categorieDao;
	
	@Transactional(rollbackFor = IdCategorieInexistantException.class)
	@Override
	public void updateCategorie(Categorie categorie) throws IdCategorieInexistantException{
		
		Categorie categorieExistante = categorieDao.getCategorieById(categorie.getId());
		if(categorieExistante == null) {
			throw new IdCategorieInexistantException();
		}
		categorieExistante.setNom(categorie.getNom());
		
	}
	
	@Override
	public Categorie getCategorieById(long id) throws IdCategorieInexistantException{
		Categorie cat = new Categorie();
		cat = categorieDao.getCategorieById(id);
		
		if(cat == null) {
			throw new IdCategorieInexistantException();
		}
		return cat;
	}
	
	@Override
	public List<Categorie> getAllCategorie() {
		
		return categorieDao.getAllCategorie();
	}
	
	@Transactional
	@Override
	public void deleteCategorieById(long id) {
		
		categorieDao.deleteCategorieById(id);
	}
	
	public void createCategorie(Categorie newCategorie) {
		
		categorieDao.createCategorie(newCategorie);
	}
	
}
