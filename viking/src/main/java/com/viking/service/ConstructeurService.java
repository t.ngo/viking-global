package com.viking.service;

import java.util.List;

import com.viking.entity.Constructeur;

public interface ConstructeurService {

	public List<Constructeur> getAllConstructeur();
}
