package com.viking.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.viking.dao.PanierDao;
import com.viking.entity.Article;
import com.viking.entity.Panier;

@Service
public class PanierServiceImpl implements PanierService{
	
	@Autowired
	private PanierDao panierDao;
	
	@Override
	public void createPanier(Panier panier) {
		panierDao.createPanier(panier);
	}
	
	@Transactional
	@Override
	public void deletePanier(long id) {
		panierDao.deletePanier(id);
	}
	
	@Transactional
	@Override
	public void addArticle(long id, Article art) {
		Panier pan = panierDao.getPanier(id);
		List<Article> listArt = pan.getArticles();
		listArt.add(art);
		pan.setArticles(listArt);
	}
	
	@Transactional
	@Override
	public void removeArticle(long id, long idArt) {
		Panier pan = panierDao.getPanier(id);
		List<Article> listArt = pan.getArticles();
		Iterator<Article> it = listArt.iterator();
		
		while(it.hasNext()) {
			Article article = it.next();
			if(article.getId() == idArt) {
				it.remove();
			}
		}

	}
	
	@Transactional
	@Override
	public void clearPanier(long id) {
		panierDao.clearPanier(id);
	}
	
	@Override
	public Panier getPanier(long idPanier) {
		return panierDao.getPanier(idPanier);
	}
	
	@Override
	public void validatePanier(Long idPanier) {
		
	}
	
	@Override
	public List<Article> getPanierList(long id) {
		return panierDao.getPanierList(id);
	}
	
}
