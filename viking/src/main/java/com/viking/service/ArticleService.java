package com.viking.service;

import java.util.List;

import com.viking.entity.Article;

public interface ArticleService {
	
	public Article getArticleById(Long id);
	public List<Article> getAllArticleByCategorie(String nomCategorie);
	public List<Article> getAllArticle();
}
