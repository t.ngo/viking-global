package com.viking.service;

import java.util.List;

import com.viking.entity.Categorie;

public interface CategorieService {
	
	public void updateCategorie(Categorie categorie) throws IdCategorieInexistantException;
	public List<Categorie> getAllCategorie();
	public Categorie getCategorieById(long id) throws IdCategorieInexistantException;
	public void deleteCategorieById(long id);
	public void createCategorie(Categorie newCategorie);

}
