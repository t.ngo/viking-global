package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viking.dao.ConstructeurDao;
import com.viking.entity.Constructeur;

@Service
public class ConstructeurServiceImpl implements ConstructeurService {

	@Autowired
	private ConstructeurDao constructeurDao;
	
	@Override
	public List<Constructeur> getAllConstructeur() {
		// TODO Auto-generated method stub
		return constructeurDao.getAllConstructeur();
	}

}
