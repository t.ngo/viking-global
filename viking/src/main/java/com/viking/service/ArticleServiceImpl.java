package com.viking.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.viking.dao.ArticleDao;
import com.viking.entity.Article;

@Service
public class ArticleServiceImpl implements ArticleService {
	
	@Autowired
	private ArticleDao articleDao;

	@Override
	public Article getArticleById(Long id) {
		// TODO Auto-generated method stub
		return articleDao.getArticleById(id);
	}

	@Override
	public List<Article> getAllArticleByCategorie(String nomCategorie) {
		// TODO Auto-generated method stub
		return articleDao.getAllArticleByCategorie(nomCategorie);
	}
	
	@Override
	public List<Article> getAllArticle() {
		return articleDao.getAllArticle();
	}

}
