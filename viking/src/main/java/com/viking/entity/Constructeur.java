package com.viking.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "constructeur")
@SequenceGenerator(name = "constructeur_seq", allocationSize = 1)

public class Constructeur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "constructeur_seq")
	private Long id;
	
	@JsonIgnore
	@OneToMany(mappedBy = "constructeur")
	private List<Article> articles;
	
	private String nom;
	
	public Constructeur() {
		
	}
	
	public Constructeur(Long id, List<Article> articles, String nom) {
		super();
		this.id = id;
		this.articles = articles;
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	

}
