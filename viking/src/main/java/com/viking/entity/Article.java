package com.viking.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@Entity
@Table(name = "article")
@SequenceGenerator(name = "article_seq", allocationSize = 1)

public class Article {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "article_seq")
	private Long id;
	private String designation;
	
	private String image;
	
	private double prix;
	
	@Column(name = "content")
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "categorie_id")
	private Categorie categorie;
	
	@ManyToOne
	@JoinColumn(name = "constructeur_id")
	private Constructeur constructeur;
	
	@Temporal(TemporalType.DATE)
	@JsonFormat(shape = Shape.STRING)
	private Date dateMiseEnLigne;
	
	private int stock;
	
	public Article() {
		
	}
	
	public Article(Long id, String designation, String image, double prix, String description, Categorie categorie,
			Constructeur constructeur, Date dateMiseEnLigne, int stock) {
		super();
		this.id = id;
		this.designation = designation;
		this.image = image;
		this.prix = prix;
		this.description = description;
		this.categorie = categorie;
		this.constructeur = constructeur;
		this.dateMiseEnLigne = dateMiseEnLigne;
		this.stock = stock;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Constructeur getConstructeur() {
		return constructeur;
	}

	public void setConstructeur(Constructeur constructeur) {
		this.constructeur = constructeur;
	}

	public Date getDateMiseEnLigne() {
		return dateMiseEnLigne;
	}

	public void setDateMiseEnLigne(Date dateMiseEnLigne) {
		this.dateMiseEnLigne = dateMiseEnLigne;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	
	
}
