package com.viking.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "panier")
@SequenceGenerator(name = "panier_seq", allocationSize = 1)

public class Panier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "panier_seq")
	private Long id;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "panierArticle", 
						joinColumns = @JoinColumn(name = "panier_id"),
						inverseJoinColumns = @JoinColumn(name = "article_id"))
	private List<Article> articles = new ArrayList<>();
	
	public Panier() {
		
	}

	public Panier(Long id, List<Article> articles) {
		super();
		this.id = id;
		this.articles = articles;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	
}
