package com.viking.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "categorie")
@SequenceGenerator(name = "categorie_seq", allocationSize = 1)

public class Categorie {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categorie_seq")
	private Long id;
	
	@JsonIgnore
	@OneToMany(mappedBy = "categorie")
	private List<Article> articles;

	private String nom;
	
	public Categorie() {
		
	}
	
	public Categorie(Long id, List<Article> articles, String nom) {
		super();
		this.id = id;
		this.articles = articles;
		this.nom = nom;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
