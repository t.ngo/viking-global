package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Article;
import com.viking.entity.Panier;

@Repository
public class PanierDaoImpl implements PanierDao{

	@PersistenceContext
	private EntityManager entityManager;

	public PanierDaoImpl() {
		
	}
	
	public PanierDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public Panier getPanier(long idPanier) {
		return entityManager.find(Panier.class, idPanier);
	}
	
	@Transactional
	@Override
	public void createPanier(Panier panier) {
		entityManager.persist(panier);
	}
	
	@Transactional
	@Override
	public void deletePanier(long id) {
		entityManager.createQuery("delete from Panier p where p.id = :id")
		.setParameter("id", id)
		.executeUpdate();
	}
	
	@Transactional
	@Override
	public void clearPanier(long id) {
		entityManager.createQuery("delete from PanierArticle p where p.panier_id = :id")
		.setParameter("id", id)
		.executeUpdate();
	}
	
	@Override
	public List<Article> getPanierList(long id) {	
		return entityManager.find(Panier.class, id).getArticles();
	}
	
	
}
