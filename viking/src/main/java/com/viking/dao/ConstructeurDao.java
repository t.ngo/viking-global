package com.viking.dao;

import java.util.List;

import com.viking.entity.Constructeur;

public interface ConstructeurDao {

	public List<Constructeur> getAllConstructeur();
}
