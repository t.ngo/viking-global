package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.viking.entity.Categorie;

@Repository
public class CategorieDaoImpl implements CategorieDao {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public CategorieDaoImpl() {
		
	}

	public CategorieDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public Categorie getCategorieById(Long id) {
		// TODO Auto-generated method stub
		return entityManager.find(Categorie.class, id);
	}

	@Transactional
	@Override
	public void createCategorie(Categorie cat) {
		// TODO Auto-generated method stub
			entityManager.persist(cat);
	}
	
	@Transactional
	@Override
	public void deleteCategorie(Categorie cat) {
		entityManager.remove(cat);
	}
	
	@Override
	public List<Categorie> getAllCategorie() {

		return entityManager.createQuery("select c from Categorie c order by c.nom", Categorie.class).getResultList();	
	}
	
	@Override
	public Categorie getCategorieByNom(String nom) {

			List<Categorie> categories = entityManager.createQuery("select c from Categorie c where c.nom = :nom", Categorie.class).setParameter("nom", nom).setMaxResults(1).getResultList();
			return categories.isEmpty() ? null : categories.get(0);
	}
	
	@Transactional
	@Override
	public void deleteCategorieById(Long id) {
		entityManager.createQuery("delete from Categorie c where c.id = :id")
        .setParameter("id", id)
        .executeUpdate();
	}

}
