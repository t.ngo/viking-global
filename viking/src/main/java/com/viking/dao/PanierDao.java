package com.viking.dao;

import java.util.List;

import com.viking.entity.Article;
import com.viking.entity.Panier;

public interface PanierDao {

	public void createPanier(Panier panier);
	public void deletePanier(long id);
	public Panier getPanier(long idPanier);
	public void clearPanier(long id);
	
	public List<Article> getPanierList(long id);

}
