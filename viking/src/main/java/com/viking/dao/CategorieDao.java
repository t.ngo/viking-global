package com.viking.dao;

import java.util.List;

import com.viking.entity.Categorie;

public interface CategorieDao {
	
	public Categorie getCategorieById(Long id);
	public void createCategorie(Categorie cat);
	public void deleteCategorie(Categorie cat);
	public void deleteCategorieById(Long id);
	public List<Categorie> getAllCategorie();
	public Categorie getCategorieByNom(String nom);
}
