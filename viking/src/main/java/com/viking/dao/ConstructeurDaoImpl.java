package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.viking.entity.Constructeur;

@Repository
public class ConstructeurDaoImpl implements ConstructeurDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	public ConstructeurDaoImpl() {
		
	}
	
	public ConstructeurDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Constructeur> getAllConstructeur() {
		// TODO Auto-generated method stub
		return entityManager.createQuery("select c from Constructeur c", Constructeur.class).getResultList();
	}

}
