package com.viking.dao;

import java.util.List;

import com.viking.entity.Article;

public interface ArticleDao {
	
	public Article getArticleById(Long id);
	public List<Article> getAllArticleByCategorie(String nomCategorie);
	public List<Article> getAllArticle();
}
