package com.viking.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.viking.entity.Article;

@Repository
public class ArticleDaoImpl implements ArticleDao{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public ArticleDaoImpl() {
		
	}

	public ArticleDaoImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public Article getArticleById(Long id) {
		return entityManager.find(Article.class, id);
	}
	
	@Override
	public List<Article> getAllArticleByCategorie(String nomCategorie) {
		return entityManager.createQuery("select a from Article a where a.categorie.nom = :nom", Article.class)
				.setParameter("nom", nomCategorie)
				.getResultList();
	}
	
	@Override
	public List<Article> getAllArticle() {
		return entityManager.createQuery("select a from Article a", Article.class).getResultList();
	}

}
