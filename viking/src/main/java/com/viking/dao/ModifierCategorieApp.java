package com.viking.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.viking.entity.Categorie;

public class ModifierCategorieApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("catalogue");
		EntityManager entityManager = emf.createEntityManager();
		
		try {
			Categorie cat = entityManager.find(Categorie.class, 21L);
			entityManager.getTransaction().begin();
			cat.setNom("spéciale");
			entityManager.getTransaction().commit();
		} 
		finally {
			entityManager.close();
			emf.close();
		}
		
	}

}
