package viking;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;


public class JpaTest {
	
	private static EntityManagerFactory emf;
	private EntityManager entityManager;
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@BeforeClass
	public static void creerEntityManagerFactory() throws Exception{
		
		Properties properties = new Properties();
		properties.load(new FileReader("src/main/resources/META-INF/jdbc.properties"));

		Map<String, String> jpaProperties = new HashMap<>();
		jpaProperties.put("javax.persistence.jdbc.url", properties.getProperty("jdbc.url"));
		jpaProperties.put("javax.persistence.jdbc.driver", properties.getProperty("jdbc.driver"));
		jpaProperties.put("javax.persistence.jdbc.user", properties.getProperty("jdbc.user"));
		jpaProperties.put("javax.persistence.jdbc.password", properties.getProperty("jdbc.password"));

		emf = Persistence.createEntityManagerFactory("catalogue", jpaProperties);

	}
	
	@AfterClass
	public static void fermerEntityManagerFactory() {
		if (emf != null) {
			emf.close();
		}
	}
	
	@Before
	public void creerEntityManager() {
		entityManager = emf.createEntityManager();
	}
	
	@After
	public void fermerEntityManager() {
		entityManager.close();
	}
	

}
