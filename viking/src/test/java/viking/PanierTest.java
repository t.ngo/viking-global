package viking;

import static org.junit.Assert.*;

import org.junit.Test;

import com.viking.dao.PanierDao;
import com.viking.dao.PanierDaoImpl;
import com.viking.entity.Article;
import com.viking.entity.Panier;

public class PanierTest extends JpaTest{
	
	@Test
	public void testCreatePanier() throws Exception {
		Article article = getEntityManager().find(Article.class, 1L);
		Panier panier = new Panier();
		panier.getArticles().add(article);
		PanierDao panierDao = new PanierDaoImpl(getEntityManager());
		
		panierDao.createPanier(panier);
		
		assertNotNull(panier.getId());
	}

}
