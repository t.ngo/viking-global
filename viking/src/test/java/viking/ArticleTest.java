package viking;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.viking.dao.ArticleDao;
import com.viking.dao.ArticleDaoImpl;
import com.viking.entity.Article;

public class ArticleTest extends JpaTest{

	@Test
	public void testGetArticleById() throws Exception {
		ArticleDao articleDao = new ArticleDaoImpl(getEntityManager());
		
		Article article = articleDao.getArticleById(Long.valueOf(2));
		
		assertEquals("MSI Optix MAG241C", article.getDesignation());

	}
	
	@Test
	public void testGetArticleByCategorie() throws Exception {
		ArticleDao articleDao = new ArticleDaoImpl(getEntityManager());
		
		List<Article> listArt = articleDao.getAllArticleByCategorie("écran");
		
		assertFalse(listArt.isEmpty());
		for(Article article : listArt) {
			assertEquals("écran", article.getCategorie().getNom());
		}
	}
}
