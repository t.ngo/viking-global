package viking;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import com.viking.dao.CategorieDao;
import com.viking.dao.CategorieDaoImpl;
import com.viking.entity.Categorie;

public class CategorieTest extends JpaTest{
	
	@Test
	public void testGetCategorie() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		
		Categorie categorie = categorieDao.getCategorieById(1L);
		
		assertEquals(3, categorie.getArticles().size());
		assertEquals("écran", categorie.getNom());
	}

	@Test
	public void testCreateDeleteCategorie() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		Categorie categorie = new Categorie();
		
		categorie.setNom("Catégorie fantome");
		
		getEntityManager().getTransaction().begin();
		categorieDao.createCategorie(categorie);
		categorieDao.deleteCategorie(categorie);
		getEntityManager().getTransaction().commit();
		
		Categorie categorieSupprimee = categorieDao.getCategorieById((categorie.getId()));
		
		assertNotNull(categorie.getId());
		assertNull(categorieSupprimee);
	}
	
	@Test
	public void testGetAllCategorie() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		
		List<Categorie> categories = categorieDao.getAllCategorie();
		
		assertFalse(categories.isEmpty());
	}

	@Test
	public void testGetCategorieByNom() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		
		Categorie categorie = categorieDao.getCategorieByNom("écran");
		
		assertNotNull(categorie);
		assertEquals("écran", categorie.getNom());
	}

	@Test
	public void testGetCategorieByNomQuandLaCategorieNexistePas() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		
		Categorie categorie = categorieDao.getCategorieByNom("xxxxxxxxxxx");
		
		assertNull(categorie);
	}
	
	@Test
	public void testDeleteCategorieById() throws Exception {
		CategorieDao categorieDao = new CategorieDaoImpl(getEntityManager());
		Categorie categorie = new Categorie();
		getEntityManager().getTransaction().begin();
		categorie.setNom("new cat");
		categorieDao.createCategorie(categorie);
		categorieDao.deleteCategorieById(categorie.getId());
		getEntityManager().getTransaction().commit();
		getEntityManager().clear();
		Categorie newCat = categorieDao.getCategorieById(categorie.getId());
		
		assertNull(newCat);
		
	}

}
