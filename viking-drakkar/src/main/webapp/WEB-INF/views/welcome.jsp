<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome</title>
</head>
<body>
	<form:form modelAttribute="rechercheParCategorie">
	
		<form:input path="nomCategorie"/>
		<form:button>Rechercher</form:button>
		
	</form:form>
</body>
</html>