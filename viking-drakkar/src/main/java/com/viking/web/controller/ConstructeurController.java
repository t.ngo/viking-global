package com.viking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viking.dao.ConstructeurDao;
import com.viking.entity.Constructeur;
import com.viking.service.ConstructeurService;

@RestController
public class ConstructeurController {

	@Autowired
	private ConstructeurDao constructeurDao;
	
	@Autowired
	private ConstructeurService constructeurService;
	
	@GetMapping("/constructeur")
	public List<Constructeur> getAllConstructeur() {
		
		return constructeurDao.getAllConstructeur();
	}
}
