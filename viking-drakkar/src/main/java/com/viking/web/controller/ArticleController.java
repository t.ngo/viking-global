package com.viking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viking.dao.ArticleDao;
import com.viking.entity.Article;
import com.viking.service.ArticleService;

@RestController
public class ArticleController {
	
	@Autowired
	private ArticleDao articleDao;
	
	@Autowired
	private ArticleService articleService;
	
	@GetMapping("/listeArticles")
	public List<Article> getAllArticle() {
		return articleDao.getAllArticle();
	}
}
