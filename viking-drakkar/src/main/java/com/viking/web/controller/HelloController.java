package com.viking.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public class HelloController {

	@GetMapping("/hello")
	public String get(@RequestParam (name="nom") String name) {
		return "hello " + name;
	}
}
