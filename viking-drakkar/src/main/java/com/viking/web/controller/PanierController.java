package com.viking.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.viking.entity.Article;
import com.viking.entity.Panier;
import com.viking.service.PanierService;

@RestController
public class PanierController {
	
	@Autowired
	private PanierService panierService;
	
	@GetMapping("/panier/{idPanier}")
	public Panier getPanier(@PathVariable long idPanier) {
		return panierService.getPanier(idPanier);
	}
	
	@PutMapping("/panier/addArticles/{idPanier}")
	public void addArticle(@PathVariable long idPanier, @RequestBody Article art) {
		System.out.println("article :"+art.getDesignation());
		panierService.addArticle(idPanier, art);
	}
	
	@DeleteMapping("/panier/removeArticle/{idPanier}/{idArt}")
	public void removeArticle(@PathVariable long idPanier, @PathVariable long idArt){
		
		panierService.removeArticle(idPanier, idArt);
	}
	
	@PutMapping("/panierArticle/delete/{id}")
	public void clearPanier(@PathVariable long id) {
		panierService.clearPanier(id);
	}
	
	@GetMapping("/panier/listeArticles/{idPanier}")
	public List<Article> getPanierList(@PathVariable long idPanier) {
		return panierService.getPanierList(idPanier);
	}

}
