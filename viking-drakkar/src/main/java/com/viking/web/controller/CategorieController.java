package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.dao.CategorieDao;
import com.viking.entity.Categorie;
import com.viking.service.CategorieService;
import com.viking.service.IdCategorieInexistantException;

@RestController
public class CategorieController {
	
	@Autowired
	private CategorieDao categorieDao;
	
	@Autowired
	private CategorieService categorieService;
	
	@ExceptionHandler(IdCategorieInexistantException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public String Handle(IdCategorieInexistantException e) {
		return e.getMessage();
	}
	
	@GetMapping("/categorie/{idCategorie}")
	public Categorie getCategorieById(@PathVariable long idCategorie) throws IdCategorieInexistantException{
		
		return categorieDao.getCategorieById(idCategorie);
	}
	
	@PutMapping("/categorie/create")
	public ResponseEntity<Categorie> createCategorie(@RequestBody Categorie newCategorie, UriComponentsBuilder builder) {
		categorieDao.createCategorie(newCategorie);
		Long id = newCategorie.getId();
		URI uri = builder.pathSegment(String.valueOf(id)).build().toUri();
		return ResponseEntity.created(uri).body(newCategorie);
	}
	
	@GetMapping("/categorie")
	public List<Categorie> getAllCategorie() {
		
		return categorieDao.getAllCategorie();
	}
	
	@DeleteMapping("/categorie/delete/{idCategorie}")
	public String deleteCategorie(@RequestParam long idCategorie) {
		
		categorieDao.deleteCategorieById(idCategorie);
		return "catégorie supprimée";
	}
	
	@PutMapping("/categorie/update/{idCategorie}")
	public Categorie updateCategorie(@PathVariable long idCategorie, @RequestBody Categorie newCategorie) throws IdCategorieInexistantException {
		newCategorie.setId(idCategorie);
		categorieService.updateCategorie(newCategorie);
		return newCategorie;
	}
}
