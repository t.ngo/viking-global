import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/models/article';
import { Panier } from 'src/app/models/panier';
import { PanierService } from 'src/app/services/panier.service';
import { ActivatedRoute } from '@angular/router';
import { ListeArticlesComponent } from 'src/app/articles/liste-articles/liste-articles.component';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  panier: Panier = new Panier();
  prixPanier: number;
  taillePanier: number;
  listeArticles: Article[];
  selectedArticle: Article;

  constructor(private panierService: PanierService, private route: ActivatedRoute) {

   }

  ngOnInit() {
    this.panierService.getPanierList(21)
    .subscribe( data => {
      this.panier.listeArticles = data;
      this.calculPrixTotal();
    });
  }

  calculPrixTotal(): void {
    let nombreArticlesPanier: number = this.panier.listeArticles.length;
    this.prixPanier = 0;
    for (let i = 0; i < nombreArticlesPanier; i++) {
      this.prixPanier += this.panier.listeArticles[i].prix;
    }
    this.taillePanier = this.panier.listeArticles.length;
  }

  onClick(article: Article): void {
    this.selectedArticle = article;
    console.log(this.selectedArticle);
    this.panierService.removeArticle(21, article);
    
  }

}
