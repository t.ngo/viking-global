import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Panier } from '../models/panier';
import { map, catchError } from 'rxjs/operators';
import { Article } from '../models/article';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  listeArticles: Article[] = [];
  public panier = new ReplaySubject<Panier>();

  baseUrl: string = 'http://localhost:8080/viking-drakkar';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getPanier(id: number) {
    id = 21;
    return this.http.get<Panier>(this.baseUrl + `/panier/${id}`, this.httpOptions).pipe(
      map(
        jsonItem => Panier.fromJson(jsonItem)
      )
    );
  }

  getPanierList(id: number) {
    id = 21;
    return this.http.get<Article[]>(this.baseUrl + `/panier/listeArticles/${id}`, this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Article.fromJson(jsonItem))
      )
    );
  }

  addArticle(id: number, article: Article) {
    console.log('addArticle appelé' + article.designation + 'panier' + id);
    return this.http.put<Panier>(this.baseUrl  + `/panier/addArticles/${id}`, article)
    .subscribe(res => {
      console.log(res);
    });
  }

  removeArticle(id: number, article: Article) {
    console.log('removeArticle appelé' + article.designation + 'panier' + id);
    this.http.delete(this.baseUrl  + `/panier/removeArticle/${id}/${article.id}`).subscribe();
  }
}

