import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';

import { Constructeur } from '../models/constructeur';
@Injectable({
  providedIn: 'root'
})
export class ConstructeurService {

  baseUrl: string = 'http://localhost:8080/viking-drakkar';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getConstructeur() {
    return this.http.get<Constructeur[]>(this.baseUrl + '/constructeur', this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Constructeur.fromjson(jsonItem))
      )
    );
  }
}

