import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Article } from '../models/article';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  baseUrl: string = 'http://localhost:8080/viking-drakkar';
  constructor(private http: HttpClient) {}

  bddUrl = 'http://localhost:8080/listeArticles';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getArticle(id: number) {
    return this.http
      .get<Article>(this.baseUrl + `/listeArticles/${id}`, this.httpOptions)
      .pipe(map(jsonItem => Article.fromJson(jsonItem)));
  }

  getListeArticles() {
    return this.http
      .get<Article[]>(this.baseUrl + '/listeArticles', this.httpOptions)
      .pipe(
        map((jsonArray: Object[]) =>
          jsonArray.map(jsonItem => Article.fromJson(jsonItem))
        )
      );
  }

  createArticle(article: Article): void {
    this.http.post<Article>(this.baseUrl + '/listeArticle/createArticle', article);
  }

}
