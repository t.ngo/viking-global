import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Categorie } from '../models/categorie';
@Injectable({
  providedIn: 'root'
})

export class CategorieService {

  baseUrl: string = 'http://localhost:8080/viking-drakkar';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getCategories() {
    return this.http.get<Categorie[]>(this.baseUrl + '/categorie', this.httpOptions).pipe(
      map(
        (jsonArray: Object[]) => jsonArray.map(jsonItem => Categorie.fromJson(jsonItem))
      )
    );
  }
}