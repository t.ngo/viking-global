import { NewArticleComponent } from './articles/new-article/new-article.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PanierComponent } from './paniers/panier/panier.component';
import { ListeArticlesComponent } from './articles/liste-articles/liste-articles.component';

const routes: Routes = [
  { path: '', component: ListeArticlesComponent },
  { path: 'panier', component: PanierComponent },
  { path: 'add-article', component: NewArticleComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
