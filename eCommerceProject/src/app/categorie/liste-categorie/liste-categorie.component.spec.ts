import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeCategorieComponent } from './liste-categorie.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ListeCategorieComponent', () => {
  let component: ListeCategorieComponent;
  let fixture: ComponentFixture<ListeCategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeCategorieComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeCategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
