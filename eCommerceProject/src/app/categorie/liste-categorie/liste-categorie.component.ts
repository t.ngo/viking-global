import { CategorieService } from '../../services/categorie.service';
import { Component, OnInit } from '@angular/core';
import { Categorie } from 'src/app/models/categorie';

@Component({
  selector: 'app-liste-categorie',
  templateUrl: './liste-categorie.component.html',
  styleUrls: ['./liste-categorie.component.css']
})
export class ListeCategorieComponent implements OnInit {

  listeCategories: Categorie[];

  constructor(private categorieService: CategorieService) { }

  ngOnInit() {
    this.categorieService.getCategories()
    .subscribe( data => {
      this.listeCategories = data;
    });
  }

}
