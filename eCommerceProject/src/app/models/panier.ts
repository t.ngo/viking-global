import { Article } from './article';

export class Panier {

    constructor(
        public id?: number,
        public listeArticles?: Article[]
        ) {}

    public static fromJson(json: Object): Panier {
        return new Panier(
            json['id'],
            json['listeArticles']
        );
    }
}
