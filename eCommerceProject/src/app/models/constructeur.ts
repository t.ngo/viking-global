export class Constructeur {

  constructor(public id: number,
        public nom: string) {}

  public static fromjson(json: Object): Constructeur {
    return new Constructeur(
      json['id'],
      json['nom']
    );
  }
}

