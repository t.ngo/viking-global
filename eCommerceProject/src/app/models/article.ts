import { Categorie } from './categorie';
import { Constructeur } from './constructeur';
export class Article {

    constructor(
      public id?: number,
      public designation?: string,
      public image?: string,
      public prix?: number,
      public content?: string,
      public categorie?: Categorie,
      public constructeur?: Constructeur,
      public dateMiseEnLigne?: Date,
      public stock?: number
      ) {}

  public static fromJson(json: Object): Article {
    return new Article(
        json['id'],
        json['designation'],
        json['image'],
        json['prix'],
        json['content'],
        json['categorie'],
        json['constructeur'],
        json['dateMiseEnLigne'],
        json['stock']
    );
  }

  }
