import { Constructeur } from 'src/app/models/constructeur';
import { Component, OnInit } from '@angular/core';
import { ConstructeurService } from 'src/app/services/constructeur.service';

@Component({
  selector: 'app-liste-constructeur',
  templateUrl: './liste-constructeur.component.html',
  styleUrls: ['./liste-constructeur.component.css']
})
export class ListeConstructeurComponent implements OnInit {

  listeConstructeurs: Constructeur[];

  constructor(private constructeurService: ConstructeurService) {}

  ngOnInit() {
    this.constructeurService.getConstructeur().subscribe(data => {
      this.listeConstructeurs = data;

    });
  }
}
