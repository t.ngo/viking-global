import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeConstructeurComponent } from './liste-constructeur.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ListeConstructeurComponent', () => {
  let component: ListeConstructeurComponent;
  let fixture: ComponentFixture<ListeConstructeurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeConstructeurComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeConstructeurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
