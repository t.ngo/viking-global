import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeArticlesComponent } from './liste-articles.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ListeArticlesComponent', () => {
  let component: ListeArticlesComponent;
  let fixture: ComponentFixture<ListeArticlesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeArticlesComponent ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeArticlesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
