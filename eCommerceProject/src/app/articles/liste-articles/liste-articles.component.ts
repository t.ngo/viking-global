import { Component, OnInit, Input } from '@angular/core';
import { Article } from 'src/app/models/article';
import { ArticleService } from 'src/app/services/article.service';
import { Panier } from 'src/app/models/panier';
import { PanierService } from 'src/app/services/panier.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss']
})
export class ListeArticlesComponent implements OnInit {

  listeArticles: Article[];
  selectedArticle: Article;
  @Input() article: Article;

  constructor(private articleService: ArticleService, private panierService: PanierService) { }

  ngOnInit() {
    this.articleService.getListeArticles()
    .subscribe( data => {
      this.listeArticles = data;
    });
  }
  onSelect(article: Article): void {
    this.selectedArticle = article;
  }

  onClick(article: Article): void {
    this.selectedArticle = article;
    console.log(this.selectedArticle);
    this.panierService.addArticle(21, article);
    
  }

}
