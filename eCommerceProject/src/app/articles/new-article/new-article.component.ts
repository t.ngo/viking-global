import { UploadImageService } from './../../services/upload-image.service';
import { ArticleService } from './../../services/article.service';
import { Article } from './../../models/article';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CategorieService } from './../../services/categorie.service';
import { Categorie } from './../../models/categorie';
import { ConstructeurService } from 'src/app/services/constructeur.service';
import { Constructeur } from 'src/app/models/constructeur';
import { Component, OnInit } from '@angular/core';
/**
 * @author David ba
 */
@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.scss']
})
export class NewArticleComponent implements OnInit {

  selectedFile: File;
  imgURL: any;
  listeConstructeurs: Constructeur[];
  listeCategories: Categorie[];
  submitted = false;
  error: string;

// création du formulaire et des validators
  articleForm = new FormGroup({
    designation: new FormControl('', [
      Validators.required,
      Validators.maxLength(100)
    ]),
    prix: new FormControl('', [Validators.required]),
    content: new FormControl('', [Validators.required]),
    stock: new FormControl('', [Validators.required]),
    constructeur: new FormControl('', [Validators.required]),
    categorie: new FormControl('', [Validators.required]),
    image: new FormControl('', [Validators.required])
  });


  get designation() {
    return this.articleForm.get('designation');
  }
  get prix() {
    return this.articleForm.get('prix');
  }
  get content() {
    return this.articleForm.get('content');
  }
  get stock() {
    return this.articleForm.get('stock');
  }
  get constructeur() {
    return this.articleForm.get('constructeur');
  }
  get categorie() {
    return this.articleForm.get('categorie');
  }
  get image() {
    return this.articleForm.get('image');
  }

  // injection des services dont j'ai besoin pour mon formulaire ainsi que le FormBuilder

  constructor(
    private constructeurService: ConstructeurService,
    private categorieService: CategorieService,
    private articleService: ArticleService,
    private uploadImageService: UploadImageService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {

    this.initForm();

// Pour récupérer la liste des constructeurs pour alimenter les options de la balise select constructeur dans le formulaire

    this.constructeurService.getConstructeur().subscribe(data => {
      this.listeConstructeurs = data;
    });

// Pour récupérer la liste des catégories pour alimenter les options de la balise select catégorie dans le formulaire

    this.categorieService.getCategories().subscribe(data => {
      this.listeCategories = data;
    });
  }

// initialisation du formulaire

  initForm() {
    this.articleForm = this.formBuilder.group({
      id: '',
      designation: ['', Validators.required],
      prix: ['', Validators.required],
      content: ['', Validators.required],
      stock: ['', Validators.required],
      constructeur: ['', Validators.required],
      categorie: ['', Validators.required],
      dateDeMiseEnLigne: '',
      image: ['', Validators.required]
    });
  }

// Methode qui se déclenche à l'envoie du formulaire via le bouton "créer article"

  onSubmit() {
    // console.log('image dans onSub ' + this.selectedFile.name);
    this.submitted = true;
    if (this.articleForm.valid) {
      const formValue = this.articleForm.value;
      const newArticle: Article = {
        designation: formValue.designation as string,
        prix: formValue.prix as number,
        content: formValue.content as string,
        stock: formValue.stock as number,
        constructeur: formValue.constructeur as Constructeur,
        categorie: formValue.categorie as Categorie,
        image: formValue.image as string
      };

      this.articleService.createArticle(newArticle);
      this.uploadImageService.uploadImage(this.selectedFile);
    }
  }

  // methode de David jacquot pour l'upload de l'image

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      const reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url
      this.selectedFile = event.target.files[0];
      reader.onload = event => {
        // called once readAsDataURL is completed
        this.imgURL = reader.result;
      };
    }
    console.log('url image' + this.selectedFile.name);
  }

  onConstructeur() {
  }

  onCategorie() {

  }

}
