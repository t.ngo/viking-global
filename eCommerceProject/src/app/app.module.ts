import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ListeCategorieComponent } from './categorie/liste-categorie/liste-categorie.component';
import { HttpClientModule } from '@angular/common/http';
import { ListeArticlesComponent } from './articles/liste-articles/liste-articles.component';
import { ListeConstructeurComponent } from './constructeur/liste-constructeur/liste-constructeur.component';
import { PanierComponent } from './paniers/panier/panier.component';
import { NewArticleComponent } from './articles/new-article/new-article.component';
import { MenuComponent } from './menu/menu.component';
import { ToolsModule } from './tools/tools.module';



@NgModule({
  declarations: [
    AppComponent,
    ListeArticlesComponent,
    ListeCategorieComponent,
    ListeConstructeurComponent,
    PanierComponent,
    NewArticleComponent,
    MenuComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ToolsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
